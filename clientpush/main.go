//go:generate goversioninfo
package main

import (
	"os/exec"
	"runtime"
	"bytes"

	"./internal/config"
	"./internal/input"
	"./internal/logging"
	"./internal/runner"
)

func runJps(holder input.ArgumentHolder) {
	// get `java` executable path

	var cmdArray []string
	var command *exec.Cmd

	if runtime.GOOS == "windows" {
		cmdArray = []string{"/C", config.JavaPath, "-jar", config.JpsClientBootJarPath}
		cmdArray = append(cmdArray, holder.GetArguments()...)
		command = exec.Command("cmd", cmdArray...)
	} else {
		cmdArray = []string{"-jar", config.JpsClientBootJarPath}
		cmdArray = append(cmdArray, holder.GetArguments()...)
		command = exec.Command(config.JavaPath, cmdArray...)
	}

	logging.Info.Printf("Command parameters: %v", cmdArray)
	
	runner.HideWindow(command)

	var out bytes.Buffer
	var stderr bytes.Buffer
	command.Stdout = &out
	command.Stderr = &stderr

	if err := command.Run(); err != nil {
	    logging.Error.Println("Result: %v / %v", out.String(), stderr.String())
		logging.Error.Fatalf("Failed to start UTC jar: %v", err)
	}
}

func main() {
	p := input.Parameters
	runJps(p)
}
