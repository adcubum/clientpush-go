package logging

import (
	"log"
	"os"
	"path/filepath"
)

var (
	// Warn logs warning messages
	Warn *log.Logger
	// Info logs info messages
	Info *log.Logger
	// Error logs error messages
	Error *log.Logger
)

func init() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	file, err := os.OpenFile(dir+"/clientpush.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	Info = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	Warn = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	Error = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}
