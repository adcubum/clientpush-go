// +build !windows

package runner

import "os/exec"

// HideWindow on windows machines, otherwise no-op
func HideWindow(cmd *exec.Cmd) {
	// no-op on non-windows machines
}
