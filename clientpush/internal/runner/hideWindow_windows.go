// +build windows

package runner

import (
	"os/exec"
	"syscall"
)

// HideWindow on windows machines, otherwise no-op
func HideWindow(cmd *exec.Cmd) {
	cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
}
