package config

import (
	"os/exec"
)

// JavaPath to the java executable
var JavaPath string

func initJavaConfig() {
	JavaPath, _ = exec.LookPath(config.Java.Executable.Path)
}
