package config

import (
	"os"
	"path"
	"strings"

	"../logging"
)

const defaultDirMarker = "default"

var defaultDirInUserHome = path.Join("jps", "clientpush", "client")

// JpsClientBootJarPath - path to the jps-cliet-boot.jar
var JpsClientBootJarPath string

func initJpsConfig() {
	if strings.HasPrefix(config.ClientBootJar.Path, "http") {
		clientJarDir := createAndGetClientJarsDir()
		clientJarName := path.Base(config.ClientBootJar.Path)
		JpsClientBootJarPath = path.Join(clientJarDir, clientJarName)

		if _, err := os.Stat(JpsClientBootJarPath); os.IsNotExist(err) { // if jar doesn't exist already
			if err := downloadFile(config.ClientBootJar.Path, JpsClientBootJarPath); err != nil { // download it
				logging.Error.Fatalf("Failed to download jar from URL: %v, error: %v", config.ClientBootJar.Path, err)
			}
			logging.Info.Printf("client jar downloaded from: %v", config.ClientBootJar.Path)
		}

	} else {
		JpsClientBootJarPath = config.ClientBootJar.Path
	}

	if JpsClientBootJarPath == "" {
		logging.Error.Fatal("Configuration error: Client jar path not set!")
	}

	logging.Info.Printf("client jar path: %v", JpsClientBootJarPath)
}

func createAndGetClientJarsDir() string {
	var clientJarDir string

	if config.ClientBootJar.Dir == defaultDirMarker || config.ClientBootJar.Dir == "" {
		userHome, err := os.UserHomeDir()
		if err != nil {
			logging.Error.Fatal("Configuration error: failed to fetch user home: ", err)
		}

		clientJarDir = path.Join(userHome, defaultDirInUserHome)
	} else {
		clientJarDir = config.ClientBootJar.Dir
	}

	os.MkdirAll(clientJarDir, 0700)

	return clientJarDir
}
