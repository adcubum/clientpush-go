package config

import (
	"bufio"
	"os"
	"reflect"
	"strings"
	"path/filepath"

	"../logging"
)

const configFileName = "clientpush.conf"

var config configuration

type configuration struct {
	ClientBootJar struct {
		Path string
		Dir  string
	}
	Java struct {
		Executable struct {
			Path string
		}
		VMArgs string
	}
}

func init() {
    dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		logging.Error.Fatal(err)
	}
	
	file, err := os.Open(dir+"\\"+configFileName)
	if err != nil {
		logging.Error.Fatal("can't open config file: ", err)
	}

	defer file.Close()

	config = parseConfigFile(file)

	initJpsConfig()
	initJavaConfig()
}

func parseConfigFile(file *os.File) configuration {
	preparedConfig := configuration{}
	scanner := bufio.NewScanner(file)
	lineNumber := 1
	for scanner.Scan() {
		line := scanner.Text()
		split := strings.SplitN(line, "=", 2)

		left := strings.TrimSpace(split[0])

		if len(split) == 2 && left != "" && split[1] != "" {
			right := strings.TrimSpace(split[1])
			right = strings.Trim(right, `"`) // remove quotes

			setField(&preparedConfig, left, right)
		} else if left == "" || strings.HasPrefix(left, "#") {
			// allow empty line and comment line starting with #
		} else {
			logging.Error.Fatalf("Configuration error: Invalid content in %v at line %v.", configFileName, lineNumber)
		}
		lineNumber++
	}

	if err := scanner.Err(); err != nil {
		logging.Error.Fatal("Configuration error: Failed reading configuration: ", err)
	}

	return preparedConfig
}

func setField(i *configuration, key string, value string) {
	tokens := strings.Split(key, ".")

	// pointer to struct - addressable
	ps := reflect.ValueOf(i)

	structValue := ps.Elem()
	setFieldRecursive(&structValue, tokens, value)
}

func setFieldRecursive(structValue *reflect.Value, tokens []string, value string) {
	if structValue.Kind() == reflect.Struct {
		field := caseInsenstiveFieldByName(*structValue, tokens[0])
		if field.Kind() == reflect.String && field.IsValid() && field.CanSet() {
			field.SetString(value)
		} else if field.Kind() == reflect.Struct {
			setFieldRecursive(&field, tokens[1:], value)
		}
	}
}

func caseInsenstiveFieldByName(value reflect.Value, name string) reflect.Value {
	name = strings.ToLower(name)
	return value.FieldByNameFunc(func(n string) bool { return strings.ToLower(n) == name })
}
