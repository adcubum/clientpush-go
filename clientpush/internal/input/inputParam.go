package input

import (
	b64 "encoding/base64"
	"os"
	"strconv"
	"strings"

	"../logging"
)

// Pushdata with Host, Port, Server URL and Push XML
type Pushdata struct {
	Host      string
	Port      string
	ServerURL string
	PushXML   string
}

// ArgumentHolder describes an interface to retrieve command line arguments
type ArgumentHolder interface {
	GetArguments() []string
}

// GetArguments ready for command line in an array
func (p Pushdata) GetArguments() []string {
	var arguments []string

	if p.Host != "" {
		arguments = []string{"--host", p.Host, "--port", p.Port}
	} else {
		arguments = []string{"--server-url", p.ServerURL}
	}

	if p.PushXML != "" {
		pushXMLDecoded, err := b64.StdEncoding.DecodeString(p.PushXML)
		logging.Info.Printf("PushXML encoded: %v", p.PushXML)
		if err != nil {
			logging.Error.Fatalf("Malformed pushxml! Needs to be Base64 encoded: %v", err)
		}

		arguments = append(arguments, "--request-parameter", string(pushXMLDecoded))

		logging.Info.Printf("PushXML decoded: %v", string(pushXMLDecoded))
	}

	return arguments
}

// Parameters parsed from input
var Parameters Pushdata

func init() {
	args := os.Args[1:]

	if len(args) != 1 {
		logging.Error.Fatalf("exactly one inputparam must be set, but %v are given: %v", len(args), args)
		os.Exit(1)
	}

	inputparam := args[0]

	/*  Fix for trailing "/" slash got by protocol handler */
	if strings.HasSuffix(inputparam, "/") {
		inputparam = inputparam[:len(inputparam)-1]
	}

	idxprothandler := strings.Index(inputparam, "://") + 3
	if idxprothandler <= 3 || len(inputparam) <= 3 {
		logging.Error.Fatalf("Wrong fromat for inputparam: %v", inputparam)
	}

	params := strings.Split(inputparam[idxprothandler:], "&")

	paramMap := make(map[string]string)
	for _, s := range params {
		split := strings.SplitN(s, "=", 2)
		if len(split) > 1 {
			paramMap[split[0]] = split[1]
		}
	}

	host, hostOk := paramMap["host"]
	port, portOk := paramMap["port"]
	serverURL, serverURLOk := paramMap["serverurl"]
	pxml, _ := paramMap["pushxml"]

	if portOk {
		_, err := strconv.Atoi(port)
		if err != nil {
			logging.Error.Fatalf("Port must be a number. Given: %v", port)
		}
	}

	if serverURLOk && (hostOk || portOk) {
		logging.Error.Fatalf("Only serveruRL or host/port are allowed: %v", paramMap)
	}

	if hostOk && !portOk {
		logging.Error.Fatalf("Setting host requres that port is also set: %v", paramMap)
	}

	if portOk && !hostOk {
		logging.Error.Fatalf("Setting port requres that host is also set: %v", paramMap)
	}

	Parameters = Pushdata{Host: host, Port: port, ServerURL: serverURL, PushXML: pxml}
}
